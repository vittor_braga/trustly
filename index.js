const app = require('./config/express')()
const port = app.get('port')

app.listen(port, () => {
    const controller = require('./controller/githubInfo')()
    app.route('/api/gihubinfo').get(controller.getInfo)
    console.log(`Servidor rodando na porta ${port}`)
})

// import cheerio from 'cheerio';
// import request from 'request-promise';
// // import interval from 'interval-promise';

// let responseDictionary = {}

// function main() {
//     // navigate("https://github.com/vittorbraga/estrutura-de-dados")
//     // navigate("https://github.com/danielcsrs/bot-webscraper")
//     navigate("https://github.com/apache/cordova-android/")
// }

// function navigate(url) {
//     if( url.indexOf('//github.com') === -1)  url = 'https://github.com' + url
//     // return new Promise((resolve, reject) => {
//       const options = {
//         url,
//         transform: function(body) {
//           return cheerio.load(body)
//         }
//       }
//       request.get(options)
//         .then(($) => {
//         //   resolve();
//             $('.Box-row.Box-row--focus-gray.py-2.d-flex.position-relative.js-navigation-item').each(function(){
//                 let nextUrl = getURL($(this));
//                 if(isDirectory($(this).html())) {
//                     navigate(nextUrl)
//                 }else {
//                     getInfoFile(nextUrl)
//                 }
//             })
//         })
//         .catch((err) => {
//         //   reject(err);
//             console.log(err)
//         })
//     // })
// }

// function isDirectory(html) {
//     return html.indexOf('aria-label="Directory"') !== -1
// }

// function getURL(html) {
//     return html.find('a').attr('href')
// }

// function getInfoFile(url) {
//     if( url.indexOf('//github.com') === -1)  url = 'https://github.com' + url
//     const options = {
//         url,
//         transform: function(body) {
//             return cheerio.load(body)
//         }
//     }
//     request.get(options)
//         .then(($) => {
//             console.log('----------------------------------------------------------------------------------------------------')
//             let text = $('.text-mono.f6.flex-auto.pr-3.flex-order-2.flex-md-order-1.mt-2.mt-md-0').html()
//             let lines = parseInt(text.substr(0, text.indexOf(" lines")).trim())
//             let size = text.split('</span>').pop().trim()
//             let convertedSize = sizeToBytes(size)
//             let fileName = $('.final-path').text()
//             let extension = fileName.split('.').pop()

//             // console.log(`lines ${lines}`)
//             // console.log('size '+size+' - '+convertedSize+' Bytes')
//             // console.log(`File name ${fileName}`)
//             // console.log(`Extension ${extension}`)

//             let item = responseDictionary[extension]
//             if(item === undefined) {
//                 item = {
//                     lines: 0,
//                     size: 0
//                 }
//             }
//             item.lines += lines
//             item.size += convertedSize
//             responseDictionary[extension] = item
//             console.log(responseDictionary);
//         })
//         .catch((err) => {
//             console.log(err)
//         })
// }

// function sizeToBytes(size) {
//     // Bytes, KB  , MB       , GB           , TB
//     //     1, 1024, 1.048.576, 1.073.741.824, 1.099.511.627.776

//     if(size.endsWith('Bytes')) return Number(size.replace('Bytes', '').trim())
//     if(size.endsWith('KB')) return Number(size.replace(' KB', '').trim()) * 1024
//     if(size.endsWith('MB')) return Number(size.replace(' MB', '').trim()) * 1048576
//     if(size.endsWith('GB')) return Number(size.replace(' GB', '').trim()) * 1073741824
//     if(size.endsWith('TB')) return Number(size.replace(' TB', '').trim()) * 1099511627776
//     return size
// }

// main()