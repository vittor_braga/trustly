const cheerio = require('cheerio')
const objRequest = require('request-promise')

module.exports = () => {
    const functions = {};
    functions.responseDictionary = {}
    functions.navigate = async function(url) {
        if( url.indexOf('//github.com') === -1)  url = 'https://github.com' + url
        const options = {
            url,
            transform: function(body) {
            return cheerio.load(body)
            }
        }
        await objRequest.get(options)
            .then(async ($) => {
                let nodes = $('.Box-row.Box-row--focus-gray.py-2.d-flex.position-relative.js-navigation-item');
                for(let i=0; i<nodes.length; i++) {
                    let nextUrl = functions.getURL($(nodes[i]));
                    if(functions.isDirectory($(nodes[i]).html())) {
                        await functions.navigate(nextUrl)
                    }else {
                        await functions.getInfoFile(nextUrl)
                    }
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }
    functions.isDirectory = function(html) {
        return html.indexOf('aria-label="Directory"') !== -1
    }
    functions.getURL = function(html) {
        return html.find('a').attr('href')
    }
    functions.getInfoFile = async function(url) {
        if( url.indexOf('//github.com') === -1)  url = 'https://github.com' + url
        const options = {
            url,
            transform: function(body) {
                return cheerio.load(body)
            }
        }
        await objRequest.get(options)
            .then(($) => {
                console.log('----------------------------------------------------------------------------------------------------')
                let text = $('.text-mono.f6.flex-auto.pr-3.flex-order-2.flex-md-order-1.mt-2.mt-md-0').html()
                let lines = parseInt(text.substr(0, text.indexOf(" lines")).trim())
                let size = text.split('</span>').pop().trim()
                let convertedSize = functions.sizeToBytes(size)
                let fileName = $('.final-path').text()
                let extension = fileName.split('.').pop()

                let item = functions.responseDictionary[extension]
                if(item === undefined) {
                    item = {
                        lines: 0,
                        size: 0
                    }
                }
                item.lines += lines
                item.size += convertedSize
                functions.responseDictionary[extension] = item
                console.log(functions.responseDictionary);
            })
            .catch((err) => {
                console.log(err)
            })
    }
    functions.sizeToBytes = function(size) {
        if(size.endsWith('Bytes')) return Number(size.replace('Bytes', '').trim())
        if(size.endsWith('KB')) return Number(size.replace(' KB', '').trim()) * 1024
        if(size.endsWith('MB')) return Number(size.replace(' MB', '').trim()) * 1048576
        if(size.endsWith('GB')) return Number(size.replace(' GB', '').trim()) * 1073741824
        if(size.endsWith('TB')) return Number(size.replace(' TB', '').trim()) * 1099511627776
        return size
    }

    const controller = {}
    controller.getInfo = async (req, res) => {
        await functions.navigate(req.query.url)
        res.status(200).json(functions.responseDictionary)
    }
    return controller;
}